var homeApp = angular.module('homeApp', []);

homeApp.controller('HomeController', function ($scope, $http){
    
    $scope.init = function() {
        $scope.Wires = [];
        $scope.wireCount = 0;

        $scope.wireModel.red = false;
        $scope.wireModel.blue = false;
        $scope.wireModel.led = false;
        $scope.wireModel.star = false;
        $scope.wireModel.none = false;
    }
    
    $scope.wireModel = {
        red : false,
        blue : false,
        led : false,
        star : false,
        none : false
     };
    
    
    $scope.addWire = function(wire) {
        $scope.wireCount++;

        if (wire.red && !wire.blue && !wire.led && !wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire if the last digit of the serial number is even.", "count" : "Wire: " + $scope.wireCount });   
        }
        else if (wire.red && wire.blue && !wire.led && !wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire if the last digit of the serial number is even.", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (wire.red && wire.blue && wire.led && !wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire if the last digit of the serial number is even.", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (wire.red && wire.blue && wire.led && wire.star)
        {
            $scope.Wires.push({"answer" : "Do not cut the wire. ", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (wire.red && wire.blue && !wire.led && wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire if the bomb has a parallel port.", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (wire.red && !wire.blue && wire.led && wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire if the bomb has two or more batteries.", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (wire.red && !wire.blue && wire.led && !wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire if the bomb has two or more batteries.", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (wire.red && !wire.blue && !wire.led && wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire. ", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (!wire.red && wire.blue && !wire.led && !wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire if the last digit of the serial number is even.", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (!wire.red && wire.blue && wire.led && !wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire if the bomb has a parallel port.", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (!wire.red && wire.blue && !wire.led && wire.star)
        {
            $scope.Wires.push({"answer" : "Do not cut the wire. ", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (!wire.red && wire.blue && wire.led && wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire if the bomb has a parallel port.", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (!wire.red && !wire.blue && !wire.led && wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire. ", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (!wire.red && !wire.blue && wire.led && wire.star)
        {
            $scope.Wires.push({"answer" : "Cut the wire if the bomb has two or more batteries. ", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (!wire.red && !wire.blue && wire.led && !wire.star)
        {
            $scope.Wires.push({"answer" : "Do not cut the wire. ", "count" : "Wire: " +  $scope.wireCount});
        }
        else if (!wire.red && !wire.blue && !wire.led && !wire.star && wire.none)
        {
            $scope.Wires.push({"answer" : "Cut the f***ing wire. ", "count" : "Wire: " +  $scope.wireCount});
        }
        //if you press the button without anthing populated
        else if (!wire.red && !wire.blue && !wire.led && !wire.star && !wire.none)
        {
            $scope.Wires.push({"answer" : "Cut the f***ing wire. ", "count" : "Wire: " +  $scope.wireCount});
        }
        
        $scope.wireModel.red = false;
        $scope.wireModel.blue = false;
        $scope.wireModel.led = false;
        $scope.wireModel.star = false;
        $scope.wireModel.none = false;
        
    }

    $scope.resetTheThings = function() {
        $scope.init();
    }
});
